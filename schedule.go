package taskscaler

import (
	"time"

	"gitlab.com/gitlab-org/fleeting/taskscaler/internal/cron"
)

var defaultSchedule = Schedule{
	IdleCount:   0,
	IdleTime:    5 * time.Minute,
	ScaleFactor: 0,
}

type schedules []Schedule

func (s schedules) active(t time.Time) Schedule {
	for i := len(s) - 1; i >= 0; i-- {
		for _, recurrence := range s[i].recurrence {
			if recurrence.Contains(t) {
				return s[i]
			}
		}
	}

	return defaultSchedule
}

type Schedule struct {
	Periods  []string
	Timezone string

	// IdleCount is the idle capacity that is attempted to be maintained.
	IdleCount int

	// IdleTime is how long idle capacity is kept around for, even when the
	// desired capacity has decreased.
	IdleTime time.Duration

	// ScaleFactor scales the idle capacity based on active tasks. It takes
	// affect when (active tasks * ScaleFactor) exceeds IdleCount. It can be
	// capped with ScaleFactorLimit.
	ScaleFactor float64

	// ScaleFactorLimit caps (active tasks * ScaleFactor), to limit total idle
	// capacity.
	ScaleFactorLimit int

	// When preemptive mode is enabled, reservations can only be allocated
	// from existing idle/immediately available capacity. When disabled,
	// reservations are allocated from immediately available or potential
	// capacity. This setting only takes affect when the IdleCount is > 0.
	PreemptiveMode bool

	recurrence []cron.Schedule
}

func (ts *taskscaler) ConfigureSchedule(schedules ...Schedule) error {
	for idx, setting := range schedules {
		// a schedule with no period is available for all periods
		if len(setting.Periods) == 0 {
			setting.Periods = []string{"* * * * *"}
		}

		for _, period := range setting.Periods {
			timezone := ts.opts.scheduleTimezone
			if setting.Timezone != "" {
				timezone = setting.Timezone
			}

			schedule, err := cron.Parse(period, timezone)
			if err != nil {
				return err
			}

			schedules[idx].recurrence = append(schedules[idx].recurrence, schedule)
		}
	}

	ts.mu.Lock()
	defer ts.mu.Unlock()

	ts.schedules = schedules
	ts.active = ts.schedules.active(time.Now())

	return nil
}
