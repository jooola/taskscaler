package taskscaler

import (
	"fmt"
	"time"

	"github.com/hashicorp/go-hclog"

	"gitlab.com/gitlab-org/fleeting/fleeting"
	flmetrics "gitlab.com/gitlab-org/fleeting/fleeting/metrics"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"

	"gitlab.com/gitlab-org/fleeting/taskscaler/metrics"
)

type options struct {
	logger              hclog.Logger
	maxInstances        int
	capacityPerInstance int
	maxUseCount         int
	failureThreshold    int
	scheduleTimezone    string
	settings            provider.Settings
	acquireDelay        time.Duration
	reservations        bool

	deleteInstancesOnShutdown bool

	scaleThrottleLimit int
	scaleThrottleBurst int

	metricsCollector         metrics.Collector
	fleetingMetricsCollector flmetrics.Collector

	upFn UpFunc

	// fleeting options
	updateInterval              time.Duration
	updateIntervalWhenExpecting time.Duration
	deletionRetryInterval       time.Duration
	shutdownDeletionRetries     int
	shutdownDeletionInterval    time.Duration
}

// UpFunc is called when an instance is running.
//
// The id of the instance, connection info and cause are provided. The cause
// enables the caller to determine why the instance exists: It is either
// Requested (to indicate it was created in response to scaling), PreExisted
// (to indicate it existed when we first queried the group) or Unexpected
// (typically when something out-of-band caused an instance to be created).
//
// Any acquisition keys and the used count can be returned, effectively
// restoring any state. This is useful after a crash/restart where pre-existing
// instances are discovered.
//
// If an error is returned, no state is restored and the instance is scheduled
// for deletion.
type UpFunc func(id string, info provider.ConnectInfo, cause fleeting.Cause) (keys []string, used int, err error)

// Option is an option used for configuring the autoscaler.
type Option func(*options) error

func newOptions(opts []Option) (options, error) {
	var options options

	options.logger = hclog.Default()
	options.acquireDelay = 2 * time.Second
	options.deleteInstancesOnShutdown = false

	options.updateInterval = fleeting.DefaultUpdateInterval
	options.updateIntervalWhenExpecting = fleeting.DefaultUpdateIntervalWhenExpecting
	options.deletionRetryInterval = fleeting.DefaultDeletionRetryInterval
	options.shutdownDeletionRetries = fleeting.DefaultShutdownDeletionRetries
	options.shutdownDeletionInterval = fleeting.DefaultShutdownDeletionInterval

	for _, o := range opts {
		err := o(&options)
		if err != nil {
			return options, err
		}
	}

	if options.capacityPerInstance <= 0 {
		options.capacityPerInstance = 1
	}

	// the ratelimit library handles <= 0 for limit and burst separately
	// and in a sane way, but here we additionally make scaleThrottleLimit
	// default to 100 if its unset. If the user sets it to <0, it will be
	// treated as unlimited.
	if options.scaleThrottleLimit == 0 {
		options.scaleThrottleLimit = 100
	}
	if options.scaleThrottleBurst == 0 {
		options.scaleThrottleBurst = options.maxInstances
	}

	if options.failureThreshold <= 0 {
		options.failureThreshold = 3
	}

	return options, nil
}

// WithLogger settings the logger.
func WithLogger(logger hclog.Logger) Option {
	return func(o *options) error {
		o.logger = logger
		return nil
	}
}

// WithMaxInstances sets the maximum allowed instances.
func WithMaxInstances(n int) Option {
	return func(o *options) error {
		if n < 0 {
			return fmt.Errorf("max instance count cannot be negative")
		}

		o.maxInstances = n
		return nil
	}
}

// WithCapacityPerInstance sets how many tasks can be executed concurrently per
// instance.
func WithCapacityPerInstance(n int) Option {
	return func(o *options) error {
		o.capacityPerInstance = n
		return nil
	}
}

// WithMaxUseCount sets the maximum number of times an instance can be assigned
// a task.
func WithMaxUseCount(n int) Option {
	return func(o *options) error {
		o.maxUseCount = n
		return nil
	}
}

// WithScheduleTimezone sets the schedule timezone.
func WithScheduleTimezone(tz string) Option {
	return func(o *options) error {
		_, err := time.LoadLocation(tz)
		if err != nil {
			return fmt.Errorf("schedule timezone: %w", err)
		}

		o.scheduleTimezone = tz
		return nil
	}
}

// WithInstanceUpFunc sets a callback function to be called when a new instance
// is ready to be connected to.
func WithInstanceUpFunc(fn UpFunc) Option {
	return func(o *options) error {
		o.upFn = fn
		return nil
	}
}

// WithInstanceGroupSettings passes common instance group settings to the
// configured provider.
func WithInstanceGroupSettings(settings provider.Settings) Option {
	return func(o *options) error {
		o.settings = settings
		return nil
	}
}

// WithAcquireDelay configures the delay between checking to see if an instance
// asked for is ready.
func WithAcquireDelay(delay time.Duration) Option {
	return func(o *options) error {
		o.acquireDelay = delay
		return nil
	}
}

// WithMetricsCollector sets MetricsCollector instance on the scaler.
func WithMetricsCollector(mc metrics.Collector) Option {
	return func(o *options) error {
		o.metricsCollector = mc
		return nil
	}
}

// WithFleetingMetricsCollector sets MetricsCollector instance to be passed to fleeting's provisioner.
func WithFleetingMetricsCollector(mc flmetrics.Collector) Option {
	return func(o *options) error {
		o.fleetingMetricsCollector = mc
		return nil
	}
}

// WithReservations ensures that any key provided to Acquire() has previously
// been reserved with Reserve().
func WithReservations() Option {
	return func(o *options) error {
		o.reservations = true
		return nil
	}
}

// WithDeleteInstancesOnShutdown ensures that taskscaler marks all instances to
// be removed before shutting down. This will eventually cause the instances to
// be deleted by fleeting during its shutdown.
func WithDeleteInstancesOnShutdown() Option {
	return func(o *options) error {
		o.deleteInstancesOnShutdown = true
		return nil
	}
}

// WithUpdateInterval sets how often fleeting checks for new instances.
func WithUpdateInterval(timeout time.Duration) Option {
	return func(o *options) error {
		o.updateInterval = timeout
		return nil
	}
}

// WithUpdateIntervalWhenExpecting sets how often fleeting checks for new
// instances when it is expecting an instance state change.
func WithUpdateIntervalWhenExpecting(timeout time.Duration) Option {
	return func(o *options) error {
		o.updateIntervalWhenExpecting = timeout
		return nil
	}
}

// WithDeletionRetryInterval sets how often fleeting will retry a deletion if
// it appears to be having no affect from status updates.
func WithDeletionRetryInterval(timeout time.Duration) Option {
	return func(o *options) error {
		o.deletionRetryInterval = timeout
		return nil
	}
}

// WithShutdownDeletionRetries sets how many attempts fleeting makes to ensure
// that instances pending deletion complete before shutdown.
func WithShutdownDeletionRetries(attempts int) Option {
	return func(o *options) error {
		o.shutdownDeletionRetries = attempts
		return nil
	}
}

// WithShutdownDeletionInterval sets how often fleeting will check to see if an
// has been deleted during shutdown.
func WithShutdownDeletionInterval(timeout time.Duration) Option {
	return func(o *options) error {
		o.shutdownDeletionInterval = timeout
		return nil
	}
}

// WithFailureThreshold sets the maximum number of consecutive health
// failures before an instance is replaced. Health events are reported
// by the Acquisition HealthSuccess and HealthFailure methods.
func WithFailureThreshold(t int) Option {
	return func(o *options) error {
		o.failureThreshold = t
		return nil
	}
}

// WithScaleThrottle sets the rate and burst limits to throttle
// instance scaling.
func WithScaleThrottle(limit, burst int) Option {
	return func(o *options) error {
		o.scaleThrottleLimit = limit
		o.scaleThrottleBurst = burst
		return nil
	}
}
