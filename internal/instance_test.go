package internal

import (
	"context"
	"fmt"
	"strconv"
	"testing"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/fleeting/fleeting"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
)

func TestCapacity(t *testing.T) {
	tests := []struct {
		name     string
		instance *instanceBuilder

		expectedAcquired    int
		expectedUnavailable int
	}{
		{
			name: "5/0 previously used, 2/4 acquired = acquired 2, unavailable 0",
			instance: &instanceBuilder{
				previouslyUsed:      5,
				maxUseCount:         0,
				currentlyAcquired:   2,
				capacityPerInstance: 4,
			},
			expectedAcquired:    2,
			expectedUnavailable: 0,
		},
		{
			name: "3/5 previously used, 2/2 acquired = acquired 2, unavailable 0",
			instance: &instanceBuilder{
				previouslyUsed:      3,
				maxUseCount:         5,
				currentlyAcquired:   2,
				capacityPerInstance: 2,
			},
			expectedAcquired:    2,
			expectedUnavailable: 0,
		},
		{
			name: "8/10 previously used, 2/5 acquired = acquired 2, unavailable 3",
			instance: &instanceBuilder{
				previouslyUsed:      8,
				maxUseCount:         10,
				currentlyAcquired:   2,
				capacityPerInstance: 5,
			},
			expectedAcquired:    2,
			expectedUnavailable: 3,
		},
		{
			name: "5/10 previously used, 2/5 acquired = acquired 2, unavailable 0",
			instance: &instanceBuilder{
				previouslyUsed:      5,
				maxUseCount:         10,
				currentlyAcquired:   2,
				capacityPerInstance: 5,
			},
			expectedAcquired:    2,
			expectedUnavailable: 0,
		},
		{
			name: "3/10 previously used, 2/5 acquired = acquired 2, unavailable 0",
			instance: &instanceBuilder{
				previouslyUsed:      3,
				maxUseCount:         10,
				currentlyAcquired:   2,
				capacityPerInstance: 5,
			},
			expectedAcquired:    2,
			expectedUnavailable: 0,
		},
		{
			name: "9/10 previously used, 1/1 acquired = acquired 1, unavailable 9",
			instance: &instanceBuilder{
				previouslyUsed:      9,
				maxUseCount:         1,
				currentlyAcquired:   1,
				capacityPerInstance: 1,
			},
			expectedAcquired:    1,
			expectedUnavailable: 9,
		},
		{
			name: "1/5 previously used, 1/5 acquired, removing = acquired 1, unavailable 1",
			instance: &instanceBuilder{
				previouslyUsed:      1,
				maxUseCount:         5,
				currentlyAcquired:   1,
				capacityPerInstance: 2,
				removing:            true,
			},
			expectedAcquired:    1,
			expectedUnavailable: 1,
		},
		{
			name: "1/5 previously used, 0/5 acquired, removing = acquired 0, unavailable 2",
			instance: &instanceBuilder{
				previouslyUsed:      1,
				maxUseCount:         5,
				currentlyAcquired:   0,
				capacityPerInstance: 2,
				removing:            true,
			},
			expectedAcquired:    0,
			expectedUnavailable: 2,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			instance := tc.instance.build()
			acquired, unavailable := instance.Capacity()
			if acquired != tc.expectedAcquired {
				t.Errorf("acquired expected %d, got %d", tc.expectedAcquired, acquired)
			}
			if unavailable != tc.expectedUnavailable {
				t.Errorf("unavailable expected %d, got %d", tc.expectedUnavailable, unavailable)
			}
		})
	}
}

func TestAcquire(t *testing.T) {
	tests := []struct {
		name       string
		instance   *instanceBuilder
		acquireKey string

		expectAcquireSlot    int
		expectAcquireSuccess bool
	}{{
		name: "success",
		instance: &instanceBuilder{
			previouslyUsed:      1,
			maxUseCount:         10,
			currentlyAcquired:   0,
			capacityPerInstance: 2,
			preparing:           false,
			ready:               true,
			removing:            false,
		},
		expectAcquireSuccess: true,
		expectAcquireSlot:    0,
	}, {
		name: "removing instance",
		instance: &instanceBuilder{
			removing: true,
			ready:    true,
		},
		acquireKey:           "abc",
		expectAcquireSuccess: false,
	}, {
		name: "unready",
		instance: &instanceBuilder{
			removing: false,
			ready:    false,
		},
		acquireKey:           "abc",
		expectAcquireSuccess: false,
	}, {
		name: "no available slots",
		instance: &instanceBuilder{
			ready:               true,
			capacityPerInstance: 2,
			currentlyAcquired:   2,
		},
		acquireKey:           "abc",
		expectAcquireSuccess: false,
	}, {
		name: "last available slot",
		instance: &instanceBuilder{
			ready:               true,
			capacityPerInstance: 2,
			currentlyAcquired:   1,
		},
		acquireKey:           "abc",
		expectAcquireSuccess: true,
		expectAcquireSlot:    1,
	}, {
		name: "maximum use",
		instance: &instanceBuilder{
			ready:               true,
			capacityPerInstance: 2,
			maxUseCount:         10,
			previouslyUsed:      10,
		},
		acquireKey:           "abc",
		expectAcquireSuccess: false,
	}, {
		name: "last use",
		instance: &instanceBuilder{
			ready:               true,
			capacityPerInstance: 2,
			maxUseCount:         10,
			previouslyUsed:      9,
		},
		acquireKey:           "abc",
		expectAcquireSuccess: true,
		expectAcquireSlot:    0,
	}, {
		name: "find a slot in the middle",
		instance: &instanceBuilder{
			ready:               true,
			capacityPerInstance: 4,
			maxUseCount:         10,
			// setup the slot scenario
			acquiredSlots: map[int]string{
				0: "abc",
				2: "def",
			},
		},
		acquireKey:           "ghi",
		expectAcquireSuccess: true,
		expectAcquireSlot:    1,
	}}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			instance := tc.instance.build()
			slot, success := instance.Acquire(tc.acquireKey)
			assert.Equal(t, tc.expectAcquireSlot, slot)
			assert.Equal(t, tc.expectAcquireSuccess, success)
		})
	}
}

func TestRelinquish(t *testing.T) {
	tests := []struct {
		name           string
		instance       *instanceBuilder
		relinquishSlot int
		lastUsedAt     time.Time

		expectDelete           bool
		expectAcquiredCount    int
		checkLastUsedAtUpdated bool
	}{{
		name: "success",
		instance: &instanceBuilder{
			ready:             true,
			maxUseCount:       10,
			currentlyAcquired: 1,
		},
		relinquishSlot:      0,
		expectAcquiredCount: 0,
	}, {
		name: "remove at maximum use count",
		instance: &instanceBuilder{
			ready:             true,
			maxUseCount:       10,
			previouslyUsed:    10,
			currentlyAcquired: 1,
		},
		relinquishSlot:      0,
		expectDelete:        true,
		expectAcquiredCount: 0,
	}, {
		name: "do not remove at maximum use count when still acquired",
		instance: &instanceBuilder{
			ready:             true,
			maxUseCount:       10,
			previouslyUsed:    10,
			currentlyAcquired: 2,
		},
		relinquishSlot:      0,
		expectDelete:        false,
		expectAcquiredCount: 1,
	}, {
		name: "do not remove when slots still in use",
		instance: &instanceBuilder{
			ready:             true,
			maxUseCount:       10,
			previouslyUsed:    1,
			currentlyAcquired: 2,
		},
		relinquishSlot:      0,
		expectDelete:        false,
		expectAcquiredCount: 1,
	}, {
		name: "do not remove after last slot is relinquished",
		instance: &instanceBuilder{
			ready:             true,
			maxUseCount:       10,
			previouslyUsed:    1,
			currentlyAcquired: 1,
		},
		relinquishSlot:      0,
		expectDelete:        false,
		expectAcquiredCount: 0,
	}, {
		name: "relinquish slot in the middle",
		instance: &instanceBuilder{
			ready:       true,
			maxUseCount: 10,
			acquiredSlots: map[int]string{
				0: "abc",
				1: "def",
				2: "geh",
			},
		},
		relinquishSlot:      1,
		expectDelete:        false,
		expectAcquiredCount: 2,
	}, {
		name: "updates last used at",
		instance: &instanceBuilder{
			ready:             true,
			maxUseCount:       10,
			currentlyAcquired: 1,
		},
		relinquishSlot:      0,
		expectAcquiredCount: 0,
		lastUsedAt:          time.Now().Add(-time.Minute),
	}}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			fleetingInstance := &mockFleetingInstance{
				expectDelete: tc.expectDelete,
			}
			instance := tc.instance.build()
			instance.instance = fleetingInstance
			instance.Relinquish(tc.relinquishSlot)
			acquired, _ := instance.Capacity()
			assert.Equal(t, tc.expectAcquiredCount, acquired)
			assert.True(t, instance.lastUsedAt.After(tc.lastUsedAt),
				"want lastUsedAt after %v. got %v", tc.lastUsedAt, instance.lastUsedAt)
			fleetingInstance.verify(t)
		})
	}
}

func TestPrepare(t *testing.T) {
	tests := []struct {
		name       string
		instance   *instanceBuilder
		upFuncKeys []string
		upFuncUsed int
		upFuncErr  error

		expectUpFuncCall    bool
		expectReadyFuncCall bool
		expectDelete        bool
		expectReady         bool
		expectWarning       bool
	}{{
		name:                "success",
		instance:            &instanceBuilder{},
		expectUpFuncCall:    true,
		expectReadyFuncCall: true,
		expectReady:         true,
	}, {
		name:                "noop removing",
		instance:            &instanceBuilder{removing: true},
		expectUpFuncCall:    false,
		expectReadyFuncCall: false,
	}, {
		name:                "noop preparing",
		instance:            &instanceBuilder{preparing: true},
		expectUpFuncCall:    false,
		expectReadyFuncCall: false,
	}, {
		name:                "noop ready",
		instance:            &instanceBuilder{ready: true},
		expectUpFuncCall:    false,
		expectReady:         true,
		expectReadyFuncCall: false,
	}, {
		name: "extra keys warning",
		instance: &instanceBuilder{
			acquiredSlots: map[int]string{
				0: "abc",
				1: "def",
			},
		},
		upFuncKeys: []string{
			"abc",
			"def",
			"geh", // key missing acquisition
		},
		expectUpFuncCall:    true,
		expectReady:         true,
		expectWarning:       true,
		expectReadyFuncCall: true,
	}, {
		name: "no extra keys",
		instance: &instanceBuilder{
			acquiredSlots: map[int]string{
				0: "abc",
				1: "def",
			},
		},
		upFuncKeys: []string{
			"def",
			"abc",
		},
		expectUpFuncCall:    true,
		expectReady:         true,
		expectWarning:       false,
		expectReadyFuncCall: true,
	}, {
		name:                "delete on up func error",
		instance:            &instanceBuilder{},
		upFuncErr:           fmt.Errorf("no can do"),
		expectUpFuncCall:    true,
		expectReady:         false,
		expectDelete:        true,
		expectReadyFuncCall: false,
	}}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			fleetingInstance := &mockFleetingInstance{
				expectDelete: tc.expectDelete,
			}
			instance := tc.instance.build()
			instance.instance = fleetingInstance
			logger := &mockLogger{
				expectWarning: tc.expectWarning,
			}
			interceptLogger := hclog.NewInterceptLogger(nil)
			interceptLogger.RegisterSink(logger)
			instance.logger = interceptLogger
			upFunc := &mockUpFunc{
				expectCall: tc.expectUpFuncCall,
				keys:       tc.upFuncKeys,
				used:       tc.upFuncUsed,
				err:        tc.upFuncErr,
			}
			readyFunc := &mockReadyFunc{
				expectedCall: tc.expectReadyFuncCall,
			}

			instance.Prepare(context.TODO(), fleeting.CauseRequested, upFunc.fn(), readyFunc.fn())
			if !tc.instance.preparing { // if the instance was already preparing, Prepare won't change that
				waitInstanceNotPreparing(instance)
			}

			assert.Equal(t, tc.expectReady, instance.ready)
			fleetingInstance.verify(t)
			logger.verify(t)
			upFunc.verify(t)
			readyFunc.verify(t)
		})
	}
}

func TestConnectInfo(t *testing.T) {
	now := time.Now()
	offsetTime := func(add time.Duration) *time.Time {
		offset := now.Add(add)
		return &offset
	}

	tests := []struct {
		name                     string
		originalExpires          *time.Time
		expectedRefreshedExpires *time.Time
		returnsError             bool
	}{
		{
			name:                     "never expires",
			originalExpires:          nil,
			expectedRefreshedExpires: nil,
		},
		{
			name:                     "cached",
			originalExpires:          offsetTime(time.Hour),
			expectedRefreshedExpires: offsetTime(time.Hour),
		},
		{
			name:                     "almost expired, but has more than 10 seconds remaining",
			originalExpires:          offsetTime(20 * time.Second),
			expectedRefreshedExpires: offsetTime(20 * time.Second),
		},
		{
			name:                     "refresh after expiration",
			originalExpires:          offsetTime(-time.Hour),
			expectedRefreshedExpires: offsetTime(time.Hour),
		},
		{
			name:                     "almost expired, less than 10 seconds refreshes",
			originalExpires:          offsetTime(5 * time.Second),
			expectedRefreshedExpires: offsetTime(time.Hour),
		},
		{
			name:                     "failed refresh after expiration",
			originalExpires:          offsetTime(-time.Hour),
			expectedRefreshedExpires: offsetTime(time.Hour),
			returnsError:             true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			fleetingInstance := &mockFleetingInstance{connectInfoExpires: tc.expectedRefreshedExpires}
			if tc.returnsError {
				fleetingInstance.connectInfoError = assert.AnError
			}

			instance := &Instance{
				info:     provider.ConnectInfo{Expires: tc.originalExpires},
				instance: fleetingInstance,
			}

			info, err := instance.ConnectInfo(context.Background())
			if tc.returnsError {
				assert.Error(t, assert.AnError, err)
				assert.Equal(t, tc.originalExpires, info.Expires)
			} else {
				assert.NoError(t, err)
				assert.Equal(t, tc.expectedRefreshedExpires, info.Expires)
			}
		})
	}
}

func TestRemoveIfExpired(t *testing.T) {
	tests := []struct {
		name                   string
		instance               *instanceBuilder
		minIdleTime            time.Duration
		durationSinceProvision time.Duration

		expectExpired bool
		expectDelete  bool
	}{{
		name:                   "success",
		instance:               &instanceBuilder{},
		minIdleTime:            time.Minute,
		durationSinceProvision: 5 * time.Minute,
		expectExpired:          true,
		expectDelete:           true,
	}, {
		name:                   "no expire before idle time",
		instance:               &instanceBuilder{},
		minIdleTime:            time.Minute,
		durationSinceProvision: 30 * time.Second,
		expectExpired:          false,
		expectDelete:           false,
	}, {
		name: "no expire when acquired",
		instance: &instanceBuilder{
			currentlyAcquired: 1,
		},
		minIdleTime:            time.Minute,
		durationSinceProvision: 5 * time.Minute,
		expectExpired:          false,
		expectDelete:           false,
	}, {
		name: "expire at max use count",
		instance: &instanceBuilder{
			maxUseCount:    10,
			previouslyUsed: 10,
		},
		minIdleTime:   time.Minute,
		expectExpired: true,
		expectDelete:  true,
	}, {
		name: "do not expire at max use count when acquired",
		instance: &instanceBuilder{
			maxUseCount:       10,
			previouslyUsed:    10,
			currentlyAcquired: 1,
		},
		minIdleTime:   time.Minute,
		expectExpired: false,
		expectDelete:  false,
	}, {
		name: "expire when failure threshold is met",
		instance: &instanceBuilder{
			consecutiveFailureCount: 3,
		},
		minIdleTime:   time.Minute,
		expectExpired: true,
		expectDelete:  true,
	}, {
		name: "expect no expire when failure theshold is not met",
		instance: &instanceBuilder{
			consecutiveFailureCount: 2,
		},
		minIdleTime:   time.Minute,
		expectExpired: false,
		expectDelete:  false,
	}}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			fleetingInstance := &mockFleetingInstance{
				expectDelete:           tc.expectDelete,
				durationSinceProvision: tc.durationSinceProvision,
			}
			instance := tc.instance.build()
			instance.instance = fleetingInstance
			expired := instance.RemoveIfExpired(tc.minIdleTime)
			assert.Equal(t, tc.expectExpired, expired)
			fleetingInstance.verify(t)
		})
	}
}

func testAcquireExpireRace(t *testing.T) {
	instance := (&instanceBuilder{ready: true, capacityPerInstance: 1}).build()
	instance.instance = &mockFleetingInstance{}

	done := make(chan struct{})
	go func() {
		done <- struct{}{}
		for {
			select {
			case <-done:
				return
			default:
				slot, ok := instance.Acquire("test")
				if ok {
					instance.Relinquish(slot)
				}
			}
		}
	}()

	<-done // ensure go routine has started
	defer close(done)

	for i := 0; i < 1000; i++ {
		if instance.RemoveIfExpired(0) {
			instance.mu.Lock()
			require.Len(t, instance.acquiredSlots, 0)
			instance.mu.Unlock()
		}
	}
}

func TestAcquireExpireRace(t *testing.T) {
	for i := 0; i < 1000; i++ {
		testAcquireExpireRace(t)
	}
}

func waitInstanceNotPreparing(inst *Instance) {
	preparing := true
	for preparing {
		inst.mu.Lock()
		preparing = inst.preparing
		inst.mu.Unlock()
		time.Sleep(time.Millisecond)
	}
}

type instanceBuilder struct {
	previouslyUsed          int
	maxUseCount             int
	failureThreshold        int
	consecutiveFailureCount int
	currentlyAcquired       int
	capacityPerInstance     int
	preparing               bool
	ready                   bool
	removing                bool
	lastUsedAt              time.Time

	acquiredSlots map[int]string
}

func (ib *instanceBuilder) build() *Instance {
	instance := &Instance{
		// used and acquired are incremented together in a call to Acquire(), so we mimic
		// that here when setting the values.
		used:                    ib.previouslyUsed + ib.currentlyAcquired,
		maxUseCount:             ib.maxUseCount,
		capacityPerInstance:     ib.capacityPerInstance,
		failureThreshold:        3,
		consecutiveFailureCount: ib.consecutiveFailureCount,
		acquiredSlots:           map[int]string{},
		preparing:               ib.preparing,
		ready:                   ib.ready,
		removing:                ib.removing,
		lastUsedAt:              ib.lastUsedAt,
		logger:                  hclog.Default(),
		info:                    provider.ConnectInfo{},
		groupCtx:                NewCancellableGroupContext(),
	}
	if ib.acquiredSlots == nil {
		for i := 0; i < ib.currentlyAcquired; i++ {
			instance.acquiredSlots[i] = strconv.Itoa(i)
		}
	} else {
		instance.acquiredSlots = ib.acquiredSlots
	}
	return instance
}

type mockLogger struct {
	expectWarning bool
	gotWarning    bool
	warning       string
}

var _ hclog.SinkAdapter = &mockLogger{}

func (m *mockLogger) Accept(_ string, level hclog.Level, msg string, _ ...interface{}) {
	if level == hclog.Warn {
		m.gotWarning = true
		m.warning = msg
	}
}

func (m *mockLogger) verify(t *testing.T) {
	if m.expectWarning && !m.gotWarning {
		t.Errorf("wanted warning. got none")
	}
	if !m.expectWarning && m.gotWarning {
		t.Errorf("wanted no warning. got %v", m.warning)
	}
}

type mockUpFunc struct {
	expectCall bool
	called     bool
	keys       []string
	used       int
	err        error
}

func (m *mockUpFunc) fn() UpFunc {
	return func(_ string, _ provider.ConnectInfo, _ fleeting.Cause) ([]string, int, error) {
		m.called = true
		return m.keys, m.used, m.err
	}
}

func (m *mockUpFunc) verify(t *testing.T) {
	if m.expectCall && !m.called {
		t.Errorf("wanted up func call. got none")
	}
	if !m.expectCall && m.called {
		t.Errorf("wanted no up func call. got one")
	}
}

type mockReadyFunc struct {
	expectedCall bool
	called       bool
}

func (m *mockReadyFunc) fn() readyFunc {
	return func(_ time.Duration, err error) {
		if err != nil {
			return
		}
		m.called = true
	}
}

func (m *mockReadyFunc) verify(t *testing.T) {
	if m.expectedCall && !m.called {
		t.Errorf("wanted ready func call, got none")
	}

	if !m.expectedCall && m.called {
		t.Errorf("wanted no ready func call, got one")
	}
}

// TODO generate fleeting instance mock to replace this hand-made one.
type mockFleetingInstance struct {
	expectDelete           bool
	deleteCalled           bool
	durationSinceProvision time.Duration
	connectInfoExpires     *time.Time
	connectInfoError       error
}

func (m *mockFleetingInstance) Delete() {
	m.deleteCalled = true
}

func (m *mockFleetingInstance) ProvisionedAt() time.Time {
	return time.Now().Add(-1 * m.durationSinceProvision)
}

func (m *mockFleetingInstance) verify(t *testing.T) {
	t.Helper()

	if m.expectDelete && !m.deleteCalled {
		t.Errorf("wanted delete call. got none")
	}
	if !m.expectDelete && m.deleteCalled {
		t.Errorf("wanted no delete call. got one")
	}
}

// unimplemented
func (m *mockFleetingInstance) ID() string                     { return "" }
func (m *mockFleetingInstance) State() provider.State          { return provider.StateRunning }
func (m *mockFleetingInstance) Cause() fleeting.Cause          { return fleeting.CauseRequested }
func (m *mockFleetingInstance) ReadyWait(context.Context) bool { return true }
func (m *mockFleetingInstance) ConnectInfo(context.Context) (provider.ConnectInfo, error) {
	return provider.ConnectInfo{Expires: m.connectInfoExpires}, m.connectInfoError
}
func (m *mockFleetingInstance) RequestedAt() time.Time { return time.Now() }
func (m *mockFleetingInstance) UpdatedAt() time.Time   { return time.Now() }
func (m *mockFleetingInstance) DeletedAt() time.Time   { return time.Now() }
