package prometheus

import (
	"github.com/prometheus/client_golang/prometheus"
)

type factory struct {
	opts prometheus.Opts
}

func newFactory(namespace string, subsystem string, constLabels prometheus.Labels) *factory {
	return &factory{
		opts: prometheus.Opts{
			Namespace:   namespace,
			Subsystem:   subsystem,
			ConstLabels: constLabels,
		},
	}
}

func (f *factory) NewGauge(name string, help string) prometheus.Gauge {
	opts := prometheus.GaugeOpts(f.opts)
	opts.Name = name
	opts.Help = help

	return prometheus.NewGauge(opts)
}

func (f *factory) NewGaugeVec(name string, help string, labelNames []string) *prometheus.GaugeVec {
	opts := prometheus.GaugeOpts(f.opts)
	opts.Name = name
	opts.Help = help

	return prometheus.NewGaugeVec(opts, labelNames)
}

func (f *factory) NewCounter(name string, help string) prometheus.Counter {
	opts := prometheus.CounterOpts(f.opts)
	opts.Name = name
	opts.Help = help

	return prometheus.NewCounter(opts)
}

func (f *factory) NewCounterVec(name string, help string, labelNames []string) *prometheus.CounterVec {
	opts := prometheus.CounterOpts(f.opts)
	opts.Name = name
	opts.Help = help

	return prometheus.NewCounterVec(opts, labelNames)
}

func (f *factory) NewHistogram(name string, help string, buckets []float64) prometheus.Histogram {
	opts := prometheus.HistogramOpts{
		Namespace:   f.opts.Namespace,
		Subsystem:   f.opts.Subsystem,
		ConstLabels: f.opts.ConstLabels,
	}
	opts.Name = name
	opts.Help = help

	opts.Buckets = buckets

	return prometheus.NewHistogram(opts)
}
