package prometheus

import (
	"github.com/prometheus/client_golang/prometheus"
)

var defInstanceTimeBuckets = []float64{1, 5, 10, 15, 30, 45, 60, 90, 120, 180}

type Option func(*options)

type options struct {
	constLabels                  prometheus.Labels
	instanceReadinessTimeBuckets []float64
}

func loadOptions(opts *options, provided []Option) {
	opts.constLabels = prometheus.Labels{}
	opts.instanceReadinessTimeBuckets = defInstanceTimeBuckets

	for _, o := range provided {
		o(opts)
	}
}

func WithConstLabels(constLabels prometheus.Labels) Option {
	return func(o *options) {
		o.constLabels = constLabels
	}
}

func WithInstanceReadinessTimeBuckets(buckets []float64) Option {
	return func(o *options) {
		if len(buckets) == 0 {
			return
		}

		o.instanceReadinessTimeBuckets = buckets
	}
}
